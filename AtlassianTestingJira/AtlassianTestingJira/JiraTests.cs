﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using PageObject;

namespace AtlassianTestingJira
{
    [TestClass]
    public class JiraTests
    {
        private IWebDriver driver;
        private string email = "antopiscio@gmail.com";
        private string password = "friends26";


        [TestCleanup]
        public void TestCleanup()
        {
            driver.Quit();
        }
        [TestMethod]
        public void CreateNewIssue()
        {
            //create driver instance
            driver = WebDriverFactory.CreateInstance();

            //Login user
            DashboardPage homePage = loginUser(driver);

            //Go to create issue page
            CreateIssuePage createIssuePage = homePage.GoCreateIssuePage();

            //Create new issue
            Issue newIssue = new Issue();
            newIssue.summary = "JiraTesting" + Guid.NewGuid();
            newIssue.description = "Steps to reproduce: Open https://jira.atlassian.com/browse/TST using IE11. Expected result: The atlassian webpage is shown. Actual result: the url cant be open. ";
            newIssue.assignee = "Unassigned";
            newIssue.version = "Ver";
            newIssue.component = "Comp";
               
            homePage= createIssuePage.CreateIssue(newIssue.summary, newIssue.description, newIssue.component, newIssue.version, newIssue.assignee);
            Thread.Sleep(4000);
            //validate that the issue was created.
            IssueNavigatorPage issueNavigatorPage = homePage.SearchIssue(newIssue.summary);
            Assert.IsTrue(issueNavigatorPage.ValidateIssueInList(newIssue.summary));

        }


        [TestMethod]
        public void UpdateIssue()
        {
            //create driver instance
            driver = WebDriverFactory.CreateInstance();
            //Login user
            loginUser(driver);
            //Create new issue
            Issue newIssue= createNewIssue(driver);
            Thread.Sleep(4000);
            DashboardPage homePage = new DashboardPage(driver);
            IssueNavigatorPage issueNavigatorPage = homePage.SearchIssue(newIssue.summary);

            //Update summary and prioriry issue
            IssuePage issuePage = issueNavigatorPage.goToIssue();
            issuePage.UpdatePriority("Bloc");
            issuePage.UpdateSummary("New Jira Updated Bug");
            //validate new summary updated

            Assert.IsTrue(issuePage.validateUpdatedSummary("New Jira Updated Bug"));
        }

        [TestMethod]
        public void SearchIssue()
        {
            //create driver instance
            driver = WebDriverFactory.CreateInstance();
            //Login user
            DashboardPage homePage = loginUser(driver);
            //Create new issue
            Issue newIssue = createNewIssue(driver);
            Thread.Sleep(4000);
            homePage = new DashboardPage(driver);
            //search for the issue created.
            IssueNavigatorPage issueNavigatorPage= homePage.SearchIssue(newIssue.summary);
            Assert.IsTrue(issueNavigatorPage.ValidateIssueInList(newIssue.summary));

        }
        private Issue createNewIssue(IWebDriver driver)
        {
            DashboardPage homePage = new DashboardPage(driver);
            CreateIssuePage createIssuePage = homePage.GoCreateIssuePage();
            Issue newIssue = new Issue();
            newIssue.summary = "JiraTesting" + Guid.NewGuid();
            newIssue.description = "Steps to reproduce: Open https://jira.atlassian.com/browse/TST using IE11. Expected result: The atlassian webpage is shown. Actual result: the url cant be open. ";
            newIssue.assignee = "Unassigned";
            newIssue.version = "Ver";
            newIssue.component = "Comp";

            homePage = createIssuePage.CreateIssue(newIssue.summary, newIssue.description, newIssue.component, newIssue.version, newIssue.assignee);

            return newIssue;
        }

        private DashboardPage loginUser(IWebDriver driver)
        {
            DashboardPage homePage = new DashboardPage(driver);
            homePage = homePage.OpenHomePage();
            LoginPage loginPage = homePage.GoToLoginPage();
            return loginPage.LoginUser(email, password);
        }
        public class Issue
        {
            public string summary { get; set; }
            public string description { get; set; }
            public string component { get; set; }
            public string version { get; set; }
            public string assignee { get; set; }

            public Issue()
            {
                
            }
        }
    }
}

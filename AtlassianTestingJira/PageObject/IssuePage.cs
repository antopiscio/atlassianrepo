﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PageObject
{
    public class IssuePage
    {
        private IWebDriver driver;
        private string idIssue = "key-val";
        private string idSummary = "summary";
        private string classSaveButton = "aui-button submit";
        private string idPriority = "priority-field";
        private string idPriorityText = "priority-val";
        private string idSummaryText = "summary-val";
        private static WebDriverWait wait;
        public IssuePage(IWebDriver driver)
        {
            this.driver = driver;
			waitForElementDisplayed(driver, idIssue);
        }

        public string getIssueID()
        {
            return driver.FindElement(By.Id(idIssue)).Text;
        }
        public bool validateUpdatedSummary(string updateSummary)
        {
            string newTitle = driver.FindElement(By.Id(idSummaryText)).Text;
            return newTitle.Equals(updateSummary);
        }

        public void UpdateSummary(string summaryUpdate)
        {
            driver.FindElement(By.Id(idSummaryText)).Click();
            waitForElementDisplayed(driver, idSummary);
            driver.FindElement(By.Id(idSummary)).SendKeys(summaryUpdate);
            driver.FindElement(By.Id(idSummary)).SendKeys(Keys.Tab);
            driver.FindElement(By.Id(idSummary)).SendKeys(Keys.Enter);


        }
        public void UpdatePriority(string priority)
        {
            driver.FindElement(By.Id(idPriorityText)).Click();
            waitForElementDisplayed(driver, idPriority);
            driver.FindElement(By.Id(idPriority)).SendKeys(priority);
            driver.FindElement(By.Id(idPriority)).SendKeys(Keys.Tab);
            driver.FindElement(By.Id(idPriority)).SendKeys(Keys.Enter);
           

        }
        public static void waitForElementDisplayed(IWebDriver driver, string elementIdToWait)
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            wait.Until<bool>((d) => { return driver.FindElement(By.Id(elementIdToWait)).Displayed; });
        }
    }



}

﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PageObject
{
    public class LoginPage
    {
        private IWebDriver driver;
        private string idUserName = "username";
        private string idPassword = "password";
        private string idLoginButton = "login-submit";
        private static WebDriverWait wait;
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            waitForElementDisplayed(driver, idUserName);
        }

        public DashboardPage LoginUser(string user, string password)
        {
            driver.FindElement(By.Id(idUserName)).Clear();
            driver.FindElement(By.Id(idUserName)).SendKeys(user);
            driver.FindElement(By.Id(idPassword)).Clear();
            driver.FindElement(By.Id(idPassword)).SendKeys(password);
            driver.FindElement(By.Id(idLoginButton)).Click();
            return new DashboardPage(driver);
        }
        public static void waitForElementDisplayed(IWebDriver driver, string elementIdToWait)
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            wait.Until<bool>((d) => { return driver.FindElement(By.Id(elementIdToWait)).Displayed; });
        }
    }



}

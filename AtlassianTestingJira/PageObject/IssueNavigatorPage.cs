﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PageObject
{
    public class IssueNavigatorPage
    {
        private IWebDriver driver;
        private string idIssueTitle = "summary-val";
        private string issueList = "issue-link";
        private static WebDriverWait wait;
        public IssueNavigatorPage(IWebDriver driver)
        {
            this.driver = driver;
            waitForElementDisplayed(driver, idIssueTitle);
        }

        public bool ValidateIssueInList(string issueToFind)
        {
            string issueText = driver.FindElement(By.Id(idIssueTitle)).Text;
            return issueText.Equals(issueToFind);
        }

        public IssuePage goToIssue()
        {
            driver.FindElement(By.ClassName(issueList)).Click();
            return new IssuePage(driver);
        }

        public static void waitForElementDisplayed(IWebDriver driver, string elementIdToWait)
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(1000));
            wait.Until<bool>((d) => { return driver.FindElement(By.Id(elementIdToWait)).Displayed; });
        }
    }   


}

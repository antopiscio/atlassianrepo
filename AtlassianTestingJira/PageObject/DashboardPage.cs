﻿using System.Threading;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObject
{
    public class DashboardPage
    {
        private IWebDriver driver;
        private string JiraUrl = "https://jira.atlassian.com/browse/TST";
        private string loginLink = "Log In";
        private string createIssueLink = "create_link";
        private string IssueLink = "//*[@id='activity-stream']/div[2]/div[1]/a[2]";
        private string searchId = "quickSearchInput";
        public DashboardPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public DashboardPage OpenHomePage()
        {
            driver.Navigate().GoToUrl(JiraUrl);
            return new DashboardPage(driver);
        }

        public LoginPage GoToLoginPage()
        {
            driver.FindElement(By.LinkText(loginLink)).Click();
            return new LoginPage(driver);
        }

        public CreateIssuePage GoCreateIssuePage()
        {
            driver.FindElement(By.Id(createIssueLink)).Click();
            return new CreateIssuePage(driver);
        }

        public IssuePage ClickIssueLink()
        {

            driver.FindElement(By.PartialLinkText("JiraTesting")).Click();
            return new IssuePage(driver);
        }
        public IssueNavigatorPage SearchIssue(string issueSummary)
        {
            driver.FindElement(By.Id(searchId)).SendKeys(issueSummary);
            driver.FindElement(By.Id(searchId)).SendKeys(Keys.Enter);
            Thread.Sleep(4000);
            return new IssueNavigatorPage(driver);
        }
    }   


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageObject
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.IE;
    public class WebDriverFactory
    {
        public static IWebDriver CreateInstance()
        {

                    FirefoxProfile firefoxProfile = new FirefoxProfile(); ;
                    var driverForReturn = firefoxProfile == null
                                                  ? new FirefoxDriver()
                                                  : new FirefoxDriver(firefoxProfile);
                        driverForReturn.Manage().Window.Maximize();
                        return driverForReturn;
          
            }
        }
    
}

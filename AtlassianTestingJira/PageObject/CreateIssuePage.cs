﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PageObject
{
    public class CreateIssuePage
    {
        private IWebDriver driver;
        private string issueTypeComboId = "issuetype-field";
        private string idSummary = "summary";
        private string idDescription = "description";
        private string idComponent = "components-textarea";
        private string idVersion = "versions-textarea";
        private string idAssigneeField = "assignee-field";
        private string componentCombo = "//*[@id='components-multi-select']/span";
        private string affectedVersionCombo = "#versions-multi-select > span";
        private string idCreateButton = "create-issue-submit";
        private static WebDriverWait wait;
        public CreateIssuePage(IWebDriver driver)
        {
            this.driver = driver;
            waitForElementDisplayed(driver, idSummary);
        }

        public DashboardPage CreateIssue(string summary, string description, string component, string version, string assignee)
        {
            driver.FindElement(By.Id(issueTypeComboId)).Clear();
            driver.FindElement(By.Id(issueTypeComboId)).SendKeys("Bug");
            driver.FindElement(By.Id(issueTypeComboId)).SendKeys(Keys.Tab);
            Thread.Sleep(4000);
            driver.FindElement(By.Id(idSummary)).SendKeys(summary);
            driver.FindElement(By.Id(idDescription)).SendKeys(description);
            driver.FindElement(By.Id(idComponent)).SendKeys(component);
            driver.FindElement(By.Id(idComponent)).SendKeys(Keys.Tab);
            driver.FindElement(By.Id(idVersion)).SendKeys(version);
            driver.FindElement(By.Id(idVersion)).SendKeys(Keys.Tab);
            driver.FindElement(By.Id(idAssigneeField)).Clear();
            driver.FindElement(By.Id(idAssigneeField)).SendKeys(assignee);
            driver.FindElement(By.Id(idAssigneeField)).SendKeys(Keys.Tab);
            driver.FindElement(By.Id(idCreateButton)).Click();
            return new DashboardPage(driver);
        }

        public static void waitForElementDisplayed(IWebDriver driver, string elementIdToWait)
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(4000));
            wait.Until<bool>((d) => { return driver.FindElement(By.Id(elementIdToWait)).Displayed; });
        }
    }
}
